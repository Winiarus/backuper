package backuper_server;

import java.io.IOException;
import java.net.Socket;

/**
 * Główna klasa programu serwera
 */
public class Backuper_Server {

    public final static int PORT = 4000;
    private static Socket socket;

    /**
     * Główna metoda programu Servera
     * @param args 
     */
    public static void main(String[] args) {
        Thread threadServer = new Thread() {
            public void run() {
                DB db = new DB();
                try {
                    Server server = new Server(PORT);
                    server.start(db);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        };
        threadServer.start();
    }
}
