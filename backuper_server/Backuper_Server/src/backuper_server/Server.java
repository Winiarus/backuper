/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package backuper_server;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * Klasa reprezentująca serwer połączenia poprzez gniazda
 */
public final class Server {

    public ServerSocket serverSocket;
    private static Socket socket;
    public static String login;

    /**
     * konstruktor parametrowy klasy Server
     * @param port
     * @throws IOException 
     */
    public Server(int port) throws IOException {
        serverSocket = new ServerSocket(port);
    }

    /**
     * metoda startująca serwer
     * @param db
     * @throws IOException 
     */
    public void start(DB db) throws IOException {
        socket = serverSocket.accept();
        try {
            while (true) {
                String returnMessage = "";
                String[] messageArray;
                String message = receiveMessage(socket);
                messageArray = message.split(">>");
               
                switch (messageArray[0]) {
                    case "login":
                        login = messageArray[1];
                        String pass = messageArray[2];
                        if (db.selectUser(login, pass)) {
                            returnMessage = "true\n";
                            new File(FtpServer.serverPath + "/" + login).mkdirs();
                            FtpServer.serverPath += login;
                            Thread threadFtp = new Thread() {
                                public void run() {
                                    FtpServer ftp = new FtpServer(Backuper_Server.PORT);
                                }
                            };
                            threadFtp.start();
                        } else {
                            returnMessage = "false\n";
                        }
                        sendMessage(socket, returnMessage);
                        break;
                    case "fileName": {
                        String file = messageArray[1];
                        String size = messageArray[2];
                        String date = messageArray[3];
                        if (db.selectFile(file, size, date)) {
                            returnMessage = "true\n";
                        } else {
                            returnMessage = "false\n";
                        }
                        sendMessage(socket, returnMessage);
                        break;
                    }
                    case "oneFile": {
                        String file = messageArray[1];
                         if (db.oneFile(file)) {
                            returnMessage = "true\n";
                        } else {
                            returnMessage = "false\n";
                        }
                        sendMessage(socket, returnMessage);
                        break;
                    }
                    case "addFile": {
                        String user = messageArray[1];
                        String file = messageArray[2];
                        String size = messageArray[3];
                        String date = messageArray[4];
                        if (db.addFile(user, file, size, date)) {
                            returnMessage = "true\n";
                        } else {
                            returnMessage = "false\n";
                        }
                        sendMessage(socket, returnMessage);
                        break;
                    }
                    case "updateFile":
                        String user = messageArray[1];
                        String file = messageArray[2];
                        String size = messageArray[3];
                        String date = messageArray[4];
                        if (db.updateFile(user, file, size, date)) {
                            returnMessage = "true\n";
                        } else {
                            returnMessage = "false\n";
                        }
                        sendMessage(socket, returnMessage);
                        break;
                    case "filesFromDB":
                        returnMessage = db.selectFiles(login);
                        sendMessage(socket, returnMessage);
                        break;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * metoda odczytująca wiadomość od klienta
     * @param client
     * @return
     * @throws IOException 
     */
    public String receiveMessage(Socket client) throws IOException {
        InputStream is = client.getInputStream();
        InputStreamReader isr = new InputStreamReader(is);
        BufferedReader br = new BufferedReader(isr);
        String message = br.readLine();
        return message;
    }

    /**
     * metoda wysyłająca wiadomość do klienta
     * @param client
     * @param rMessage
     * @throws IOException 
     */
    public void sendMessage(Socket client, String rMessage) throws IOException {
        OutputStream os = client.getOutputStream();
        OutputStreamWriter osw = new OutputStreamWriter(os);
        BufferedWriter bw = new BufferedWriter(osw);
        bw.write(rMessage);
        bw.flush();
    }
}
