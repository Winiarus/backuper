/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package backuper_client;

import it.sauronsoftware.cron4j.Scheduler;

/**
 * Klasa reprezentująca model cyklicznego wysyłania danych
 */
public class SchedulerCron {

    ClientFrame clientFrame = ClientFrame.getInstance();

    public static Scheduler scheduler;

    /**
     * konstruktor klasy ScheluderCron
     */
    SchedulerCron() {
        scheduler = new Scheduler();
        scheduler.schedule(clientFrame.cron, new Runnable() {
            public void run() {
                if (ClientFrame.getStopVal() == true) {
                    stopCron();
                    clientFrame.getInfoLabel().setText("Scheduler has been stopped");
                }
                clientFrame.getInfoLabel().setText("Scheduler has been working");
                clientFrame.checkClientFiles();
                FtpClient ftpClient = new FtpClient(LoginFrame.PORT, LoginFrame.HOST, false);
                clientFrame.getSchedulerButtonOn().setEnabled(false);
                clientFrame.getRecoverButton().setEnabled(false);
                clientFrame.getSchedulerButtonOff().setEnabled(true);
            }
        });

        if (ClientFrame.getStopVal() == false) {
            scheduler.start();
        }
    }

    /**
     * metoda stopująca cykliczne wysyłanie danych
     */
    public static void stopCron() {
        {
            scheduler.stop();
        }
    }
}
