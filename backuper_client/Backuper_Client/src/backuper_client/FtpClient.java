package backuper_client;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import javax.swing.table.DefaultTableModel;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTP;

/**
 * Klasa odpowiadajaca za przesylanie plikow za pomoca FTP
 */
public class FtpClient extends FTP {

    ClientFrame clientFrame = ClientFrame.getInstance();

    /**
     * konstruktor parametrowy klasy FTPClient
     * @param port
     * @param server
     * @param recover 
     */
    public FtpClient(int port, String server, boolean recover) {
        port = port + 1;
        String user = "test";
        String pass = "test";

        FTPClient ftpClient = new FTPClient();
        try {
            ftpClient.connect(server, port);
            ftpClient.login(user, pass);
            ftpClient.enterLocalPassiveMode();
            ftpClient.setFileType(FTP.BINARY_FILE_TYPE);

            if (recover) {
                for (BUFile file : clientFrame.serverFiles) {
                }
                int licznik = 0;
                for (BUFile file : clientFrame.serverFiles) {
                    String recoveryPath = System.getProperty("user.dir") + "/" + "Recovery/";
                    new File(recoveryPath).mkdirs();
                    OutputStream os = new FileOutputStream(recoveryPath + file.getFile());
                    clientFrame.getInfoLabel().setText("Recovering file: " + file.getFile() + " in progress");
                    boolean done = ftpClient.retrieveFile(file.getFile(), os);
                    os.close();
                    if (done) {
                        licznik++;
                    }
                }
                if (clientFrame.serverFiles.size() == licznik) {
                    clientFrame.getInfoLabel().setText("Files have been recovered");
                    clientFrame.getSchedulerButtonOn().setEnabled(true);
                    clientFrame.getBackupButton().setEnabled(true);
                } else {
                    clientFrame.getInfoLabel().setText("Files can't be recovered");
                    clientFrame.getSchedulerButtonOn().setEnabled(true);
                    clientFrame.getBackupButton().setEnabled(true);
                }
            } else {
                int i=0;
                for (BUFile file : clientFrame.listOfFile) {
                    if (file.getStatus()) {
                        //PLIKT JEST AKTUALNY NA SERWERZE
                    } else {
                        File firstLocalFile = new File(file.getPath());
                        String firstRemoteFile = firstLocalFile.getName();
                        InputStream inputStream = new FileInputStream(firstLocalFile);
                        boolean done = ftpClient.storeFile(firstRemoteFile, inputStream);
                        inputStream.close();
                        if (done) {
                            if (file.getActual()) {
                                if (LoginFrame.client == null) {
                                    LoginFrame.client = new Client(LoginFrame.HOST, LoginFrame.PORT);
                                }
                                String message = "updateFile>>" + file.getUser() + ">>" + file.getFile() + ">>" + file.getSize() + ">>" + file.getDate();
                                LoginFrame.client.sendMessage(Client.socket, message);
                                String tmp = LoginFrame.client.receiveMessage(Client.socket);
                                if (tmp.equals("true")) {
                                    DefaultTableModel clientModel = (DefaultTableModel) clientFrame.getClientTable().getModel();
                                    clientModel.setValueAt(true, i, 4);
                                    file.setStatus(true);

                                    DefaultTableModel serverModel = (DefaultTableModel) clientFrame.getServerTable().getModel();
                                    for (int j = serverModel.getRowCount(); j > 0; j--) {
                                        serverModel.removeRow(j - 1);
                                    }
                                    clientFrame.getFilesFromServerDB();
                                    clientFrame.getInfoLabel().setText("Backup is ready!");
                                    clientFrame.getSchedulerButtonOn().setEnabled(true);
                                    clientFrame.getRecoverButton().setEnabled(true);
                                } else {
                                    file.setStatus(false);
                                    clientFrame.getInfoLabel().setText("Something goes wrong...");
                                    clientFrame.getSchedulerButtonOn().setEnabled(true);
                                    clientFrame.getRecoverButton().setEnabled(true);
                                }
                            } else {
                                //Dodanie pliku do bazy danych
                                String message = "addFile>>" + file.getUser() + ">>" + file.getFile() + ">>" + file.getSize() + ">>" + file.getDate();
                                try {
                                    if (LoginFrame.client == null) {
                                        LoginFrame.client = new Client(LoginFrame.HOST, LoginFrame.PORT);
                                    }
                                    LoginFrame.client.sendMessage(Client.socket, message);
                                    String tmp = LoginFrame.client.receiveMessage(Client.socket);
                                    switch (tmp) {
                                        case "true":
                                            DefaultTableModel serverModel = (DefaultTableModel) clientFrame.getServerTable().getModel();
                                            serverModel.addRow(new Object[]{file.getFile(), file.getSize(), file.getDate()});

                                            //zmiana statusu pliku w tabeli clienta
                                            DefaultTableModel clientModel = (DefaultTableModel) clientFrame.getClientTable().getModel();
                                            clientModel.setValueAt(true, i, 4);
                                            file.setStatus(true);
                                            clientFrame.getInfoLabel().setText("Backup is ready!");
                                            clientFrame.getSchedulerButtonOn().setEnabled(true);
                                            clientFrame.getRecoverButton().setEnabled(true);
                                            break;
                                        default:
                                            break;
                                    }
                                } catch (IOException ex) {
                                }
                            }
                        }
                    }
                    i++;
                }
                clientFrame.getSchedulerButtonOn().setEnabled(true);
                clientFrame.getRecoverButton().setEnabled(true);
                clientFrame.getSchedulerButtonOff().setEnabled(false);
                clientFrame.getInfoLabel().setText("Backuper is waiting for commands");
                for (int j = clientFrame.listOfFile.size(); j > 0; j--) {
                    clientFrame.listOfFile.remove(j - 1);
                }
            }
        } catch (IOException ex) {
        } finally {
            try {
                if (ftpClient.isConnected()) {
                    ftpClient.logout();
                    ftpClient.disconnect();
                }
            } catch (IOException ex) {
            }
        }
    }

    /**
     * metoda wysyłająca foldery na serwer poprzez FTP
     * @param ftpClient
     * @param remoteDirPath
     * @param localParentDir
     * @param remoteParentDir
     * @throws IOException 
     */
    public static void uploadDirectory(FTPClient ftpClient,
            String remoteDirPath, String localParentDir, String remoteParentDir)
            throws IOException {

        File localDir = new File(localParentDir);
        File[] subFiles = localDir.listFiles();
        if (subFiles != null && subFiles.length > 0) {
            for (File item : subFiles) {
                String remoteFilePath = remoteDirPath + "/" + remoteParentDir
                        + "/" + item.getName();
                if (remoteParentDir.equals("")) {
                    remoteFilePath = remoteDirPath + "/" + item.getName();
                }

                if (item.isFile()) {
                    // upload the file
                    String localFilePath = item.getAbsolutePath();
                    boolean uploaded = uploadSingleFile(ftpClient,
                            localFilePath, remoteFilePath);
                } else {
                    // create directory on the server
                    boolean created = ftpClient.makeDirectory(remoteFilePath);

                    // upload the sub directory
                    String parent = remoteParentDir + "/" + item.getName();
                    if (remoteParentDir.equals("")) {
                        parent = item.getName();
                    }

                    localParentDir = item.getAbsolutePath();
                    uploadDirectory(ftpClient, remoteDirPath, localParentDir,
                            parent);
                }
            }
        }
    }

    /**
     * metoda wysyłająca pojedynczy plik na serwer poprzez FTP
     * @param ftpClient
     * @param localFilePath
     * @param remoteFilePath
     * @return
     * @throws IOException 
     */
    public static boolean uploadSingleFile(FTPClient ftpClient, String localFilePath, String remoteFilePath) throws IOException {
        File localFile = new File(localFilePath);

        InputStream inputStream = new FileInputStream(localFile);
        try {
            ftpClient.setFileType(FTP.BINARY_FILE_TYPE);
            System.out.println("filePath" + remoteFilePath);
            return ftpClient.storeFile(remoteFilePath, inputStream);
        } finally {
            inputStream.close();
        }
    }
}
