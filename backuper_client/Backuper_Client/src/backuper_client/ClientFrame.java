/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package backuper_client;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.table.DefaultTableModel;

/**
 * Metoda reprezentująca główne okno aplikacji klienta
 */
public class ClientFrame extends javax.swing.JFrame {

    /**
     * Singleton
     */
    private final static ClientFrame instance = new ClientFrame();

    public static ClientFrame getInstance() {
        return instance;
    }

    /**
     * Scheduler Thread
     */
    public Thread on;
    private static Boolean stopVal = false;
    public String cron;
    public ArrayList<BUFile> listOfFile = new ArrayList<>();
    public ArrayList<BUFile> serverFiles = new ArrayList<>();

    /**
     * Creates new form ClientFrame
     */
    private ClientFrame() {
        super("Backuper");
        this.setVisible(false);
        initComponents();
       // labelLogin.setText("You are logged as " + LoginFrame.LOGIN);
        Dimension dimension = Toolkit.getDefaultToolkit().getScreenSize();
        int x = (int) ((dimension.getWidth() - this.getWidth()) / 2);
        int y = (int) ((dimension.getHeight() - this.getHeight()) / 2);
        this.setLocation(x, y);
        buttonScheluderOff.setEnabled(false);
    }

    /**
     * This method is called from within the constructor to initialize the form. WARNING: Do NOT modify this code. The
     * content of this method is always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        labelLogged = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tableServer = new javax.swing.JTable();
        jScrollPane2 = new javax.swing.JScrollPane();
        tableClient = new javax.swing.JTable();
        buttonLoadFIle = new javax.swing.JButton();
        buttonBackup = new javax.swing.JButton();
        jScrollPane4 = new javax.swing.JScrollPane();
        listHour = new javax.swing.JList();
        labelHour = new javax.swing.JLabel();
        jScrollPane5 = new javax.swing.JScrollPane();
        listDayOfMonth = new javax.swing.JList();
        labelDayOfMonth = new javax.swing.JLabel();
        jScrollPane6 = new javax.swing.JScrollPane();
        listMonth = new javax.swing.JList();
        labelMonth = new javax.swing.JLabel();
        jScrollPane7 = new javax.swing.JScrollPane();
        listDayOfWeek = new javax.swing.JList();
        labelDayOfWeek = new javax.swing.JLabel();
        buttonScheluderOn = new javax.swing.JButton();
        labelMinute = new javax.swing.JLabel();
        jScrollPane8 = new javax.swing.JScrollPane();
        listMinute = new javax.swing.JList();
        buttonDelete = new javax.swing.JButton();
        buttonScheluderOff = new javax.swing.JButton();
        buttonRecover = new javax.swing.JButton();
        infoLabel = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        labelLogged.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        labelLogged.setText("You are logged as ");

        tableServer.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "File", "Size", "Date"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.String.class, java.lang.String.class, java.lang.String.class
            };
            boolean[] canEdit = new boolean [] {
                false, true, true
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane1.setViewportView(tableServer);

        tableClient.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "File", "Path", "Size", "Date", "Status"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.Boolean.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane2.setViewportView(tableClient);

        buttonLoadFIle.setFont(new java.awt.Font("Dialog", 0, 16)); // NOI18N
        buttonLoadFIle.setText("Load Files");
        buttonLoadFIle.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonLoadFIleActionPerformed(evt);
            }
        });

        buttonBackup.setFont(new java.awt.Font("Dialog", 0, 16)); // NOI18N
        buttonBackup.setText("Back up");
        buttonBackup.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonBackupActionPerformed(evt);
            }
        });

        listHour.setModel(new javax.swing.AbstractListModel() {
            String[] strings = { "Every", "0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23" };
            public int getSize() { return strings.length; }
            public Object getElementAt(int i) { return strings[i]; }
        });
        listHour.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        jScrollPane4.setViewportView(listHour);

        labelHour.setText("Hour:");

        listDayOfMonth.setModel(new javax.swing.AbstractListModel() {
            String[] strings = { "Every", "0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30", "31" };
            public int getSize() { return strings.length; }
            public Object getElementAt(int i) { return strings[i]; }
        });
        listDayOfMonth.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        jScrollPane5.setViewportView(listDayOfMonth);

        labelDayOfMonth.setText("Day of month:");

        listMonth.setModel(new javax.swing.AbstractListModel() {
            String[] strings = { "Every", "0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12" };
            public int getSize() { return strings.length; }
            public Object getElementAt(int i) { return strings[i]; }
        });
        listMonth.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        jScrollPane6.setViewportView(listMonth);

        labelMonth.setText("Month:");

        listDayOfWeek.setModel(new javax.swing.AbstractListModel() {
            String[] strings = { "Every", "Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday" };
            public int getSize() { return strings.length; }
            public Object getElementAt(int i) { return strings[i]; }
        });
        listDayOfWeek.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        jScrollPane7.setViewportView(listDayOfWeek);

        labelDayOfWeek.setText("Day of week:");

        buttonScheluderOn.setFont(new java.awt.Font("Dialog", 0, 16)); // NOI18N
        buttonScheluderOn.setText("ON");
        buttonScheluderOn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonScheluderOnActionPerformed(evt);
            }
        });

        labelMinute.setText("Minute:");

        listMinute.setModel(new javax.swing.AbstractListModel() {
            String[] strings = { "Every", "0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30", "31", "32", "33", "34", "35", "36", "37", "38", "39", "40", "41", "42", "43", "44", "45", "46", "47", "48", "49", "50", "51", "52", "53", "54", "55", "56", "57", "58", "59" };
            public int getSize() { return strings.length; }
            public Object getElementAt(int i) { return strings[i]; }
        });
        listMinute.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        jScrollPane8.setViewportView(listMinute);

        buttonDelete.setFont(new java.awt.Font("Dialog", 0, 16)); // NOI18N
        buttonDelete.setText("Delete Selected Files");
        buttonDelete.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonDeleteActionPerformed(evt);
            }
        });

        buttonScheluderOff.setFont(new java.awt.Font("Dialog", 0, 16)); // NOI18N
        buttonScheluderOff.setText("OFF");
        buttonScheluderOff.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonScheluderOffActionPerformed(evt);
            }
        });

        buttonRecover.setFont(new java.awt.Font("Dialog", 0, 16)); // NOI18N
        buttonRecover.setText("Recover Files ");
        buttonRecover.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                recoverFilesActionPerformed(evt);
            }
        });

        infoLabel.setText("Info: ");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(labelLogged, javax.swing.GroupLayout.PREFERRED_SIZE, 300, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 400, Short.MAX_VALUE)
                                .addComponent(buttonRecover, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                            .addComponent(infoLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 375, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(buttonBackup, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                .addComponent(buttonLoadFIle, javax.swing.GroupLayout.PREFERRED_SIZE, 195, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(buttonDelete, javax.swing.GroupLayout.PREFERRED_SIZE, 195, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 401, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(labelMinute, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jScrollPane4)
                            .addComponent(labelHour, javax.swing.GroupLayout.DEFAULT_SIZE, 179, Short.MAX_VALUE)
                            .addComponent(jScrollPane5)
                            .addComponent(labelDayOfMonth, javax.swing.GroupLayout.DEFAULT_SIZE, 179, Short.MAX_VALUE)
                            .addComponent(jScrollPane6)
                            .addComponent(labelMonth, javax.swing.GroupLayout.DEFAULT_SIZE, 179, Short.MAX_VALUE)
                            .addComponent(jScrollPane7)
                            .addComponent(labelDayOfWeek, javax.swing.GroupLayout.DEFAULT_SIZE, 179, Short.MAX_VALUE)
                            .addComponent(jScrollPane8, javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(buttonScheluderOn, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(buttonScheluderOff, javax.swing.GroupLayout.PREFERRED_SIZE, 81, javax.swing.GroupLayout.PREFERRED_SIZE)))))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(labelLogged)
                        .addGap(18, 18, 18)
                        .addComponent(infoLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 461, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                            .addComponent(jScrollPane2)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(buttonLoadFIle, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(buttonDelete, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGroup(layout.createSequentialGroup()
                            .addComponent(labelMinute)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addComponent(jScrollPane8, javax.swing.GroupLayout.PREFERRED_SIZE, 75, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addComponent(labelHour)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addComponent(jScrollPane4, javax.swing.GroupLayout.PREFERRED_SIZE, 75, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addComponent(labelDayOfMonth)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addComponent(jScrollPane5, javax.swing.GroupLayout.PREFERRED_SIZE, 75, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addComponent(labelMonth)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addComponent(jScrollPane6, javax.swing.GroupLayout.PREFERRED_SIZE, 75, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addComponent(labelDayOfWeek)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addComponent(jScrollPane7, javax.swing.GroupLayout.PREFERRED_SIZE, 75, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(buttonScheluderOn, javax.swing.GroupLayout.DEFAULT_SIZE, 52, Short.MAX_VALUE)
                    .addComponent(buttonScheluderOff, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(buttonRecover, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(buttonBackup, javax.swing.GroupLayout.DEFAULT_SIZE, 50, Short.MAX_VALUE))
                .addGap(7, 7, 7))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void buttonLoadFIleActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonLoadFIleActionPerformed
        JFileChooser fc = new JFileChooser();
        int returnVal = fc.showSaveDialog(this);
        if (returnVal == JFileChooser.APPROVE_OPTION) {
            File f = fc.getSelectedFile();
            String path = f.getAbsolutePath();
            String file = path.substring(path.lastIndexOf("\\") + 1, path.length());
            String size = fileSize(Double.parseDouble(String.valueOf(f.length())));

            SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
            String date = sdf.format(f.lastModified());

            DefaultTableModel model = (DefaultTableModel) tableClient.getModel();

            String message = "fileName>>" + file + ">>" + size + ">>" + date;
            Boolean flag = false;
            try {
                if (LoginFrame.client == null) {
                    LoginFrame.client = new Client(LoginFrame.HOST, LoginFrame.PORT);
                }
                LoginFrame.client.sendMessage(Client.socket, message);
                String tmp = LoginFrame.client.receiveMessage(Client.socket);
                switch (tmp) {
                    case "true":
                        flag = true;
                        break;
                    default:
                        break;
                }
            } catch (IOException ex) {
            }
            model.addRow(new Object[]{file, path, size, date, flag});
        }
    }//GEN-LAST:event_buttonLoadFIleActionPerformed

    /**
     * metoda pobierająca informacje o plikach znajdujących się na serwerze
     */
    public void getFilesFromServerDB() {
        String message = "filesFromDB>>";
        try {
            if (LoginFrame.client == null) {
                LoginFrame.client = new Client(LoginFrame.HOST, LoginFrame.PORT);
            }
            LoginFrame.client.sendMessage(Client.socket, message);
            String tmp = LoginFrame.client.receiveMessage(Client.socket);
            String[] messageArray = tmp.split(">>");
            
            int i = Integer.parseInt(messageArray[0]);
            
            if (i != 0) {
                for (int j = 1; j < messageArray.length; j++) {
                    DefaultTableModel serverModel = (DefaultTableModel) tableServer.getModel();
                    serverModel.addRow(new Object[]{messageArray[j], messageArray[++j], messageArray[++j]});
                }
            } else {
            }
        } catch (IOException ex) {

        }
    }

    /**
     * metoda zmieniająca format rozmiaru pliku
     * @param d
     * @return 
     */
    private String fileSize(double d) {
        String size = "";
        if (d > 1024 * 1024 * 1024) {
            d = d * 100 / 1024 / 1024 / 1024;
            d = (double) ((int) d);
            d /= 100;
            size = Double.toString(d) + " GB";
        } else if (d > 1024 * 1024) {
            d = d * 100 / 1024 / 1024;
            d = (double) ((int) d);
            d /= 100;
            size = Double.toString(d) + " MB";
        } else if (d > 1024) {
            d = d * 100 / 1024;
            d = (double) ((int) d);
            d /= 100;
            size = Double.toString(d) + " KB";
        } else {
            d = d * 100;
            d = (double) ((int) d);
            d /= 100;
            size = Double.toString(d) + " B";
        }
        return size;
    }

    private void buttonDeleteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonDeleteActionPerformed
        DefaultTableModel model = (DefaultTableModel) tableClient.getModel();
        int[] rows = tableClient.getSelectedRows();
        for (int i = 0; i < rows.length; i++) {
            model.removeRow(rows[i] - i);
        }
    }//GEN-LAST:event_buttonDeleteActionPerformed

    private void buttonScheluderOnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonScheluderOnActionPerformed
        String dayOfMonth = "";
        String dayOfWeek = "";
        String hour = "";
        String minute = "";
        String month = "";
        if (listDayOfMonth.getSelectedValue() != null) {
            dayOfMonth = listDayOfMonth.getSelectedValue().toString();
            if (dayOfMonth.equals("Every")) {
                dayOfMonth = "*";
            }
        } else {
            dayOfMonth = "*";
        }

        if (listDayOfWeek.getSelectedValue() != null) {
            dayOfWeek = listDayOfWeek.getSelectedValue().toString();
            if (dayOfWeek.equals("Every")) {
                dayOfWeek = "*";
            } else {
                Integer day = listDayOfWeek.getSelectedIndex() - 1;
                dayOfWeek = day.toString();
            }
        } else {
            dayOfWeek = "*";
        }

        if (listHour.getSelectedValue() != null) {
            hour = listHour.getSelectedValue().toString();
            if (hour.equals("Every")) {
                hour = "*";
            }
        } else {
            hour = "*";
        }

        if (listMinute.getSelectedValue() != null) {
            minute = listMinute.getSelectedValue().toString();
            if (minute.equals("Every")) {
                minute = "*";
            }
        } else {
            minute = "*";
        }

        if (listMonth.getSelectedValue() != null) {
            month = listMonth.getSelectedValue().toString();
            if (month.equals("Every")) {
                month = "*";
            }
        } else {
            month = "*";
        }
        cron = minute + " " + hour + " " + dayOfMonth + " " + month + " " + dayOfWeek;
        startThread();


    }//GEN-LAST:event_buttonScheluderOnActionPerformed
    /**
     * Method using to stop scheduler
     *
     * @param evt
     */
    private void buttonScheluderOffActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonScheluderOffActionPerformed
        stopThread();
    }//GEN-LAST:event_buttonScheluderOffActionPerformed

    private void buttonBackupActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonBackupActionPerformed
        buttonRecover.setEnabled(false);
        buttonScheluderOn.setEnabled(false);
        buttonScheluderOff.setEnabled(false);
        infoLabel.setForeground(Color.blue);
        infoLabel.setText("Backup is in progress");
        checkClientFiles();
        Thread threadBackup = new Thread() {
            public void run() {
                FtpClient ftpClient = new FtpClient(LoginFrame.PORT, LoginFrame.HOST, false);
            }
        };
        threadBackup.start();
    }//GEN-LAST:event_buttonBackupActionPerformed

    private void recoverFilesActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_recoverFilesActionPerformed
        buttonBackup.setEnabled(false);
        buttonScheluderOn.setEnabled(false);
        buttonScheluderOff.setEnabled(false);
        infoLabel.setForeground(Color.red);
        infoLabel.setText("Recover is in progress");
        DefaultTableModel model = (DefaultTableModel) tableServer.getModel();
        for (int i = 0; i < model.getRowCount(); i++) {
            String login = LoginFrame.LOGIN;
            String file = model.getValueAt(i, 0).toString();
            String size = model.getValueAt(i, 1).toString();
            String date = model.getValueAt(i, 2).toString();
            String path = "";
            BUFile bufile = new BUFile(login, file, size, date, path);
            serverFiles.add(bufile);
        }
        Thread threadBackup = new Thread() {
            public void run() {
                FtpClient ftpClient = new FtpClient(LoginFrame.PORT, LoginFrame.HOST, true);
            }
        };
        threadBackup.start();
    }//GEN-LAST:event_recoverFilesActionPerformed
    
    /**
     * metoda startująca wątek cyklicznego wysyłania danych
     */
    public void startThread() {
        buttonLoadFIle.setEnabled(false);
        buttonBackup.setEnabled(false);
        buttonLoadFIle.setEnabled(false);
        buttonDelete.setEnabled(false);
        buttonScheluderOn.setEnabled(false);
        buttonScheluderOff.setEnabled(true);
        buttonRecover.setEnabled(false);
        checkClientFiles();
        stopVal = false;
        on = new Thread() {
            public void run() {
                Thread thisThread = Thread.currentThread();
                try {
                    SchedulerCron scheduler = new SchedulerCron();
                } catch (Exception e) {
                } finally {

                }
            }
        };
        on.start();
    }

    /**
     * metoda sprawdzająca czy pliki w tabeli klienta uległy zmianie
     */
    public void checkClientFiles() {
        for(int i = listOfFile.size(); i>0; i--){
            listOfFile.remove(i-1);
        }
        
        DefaultTableModel model = (DefaultTableModel) tableClient.getModel();
        for (int i = 0; i < model.getRowCount(); i++) {
            String login = LoginFrame.LOGIN;
            String file = "";
            String size = "";
            String date = "";
            String path = "";
            
            File f = new File(model.getValueAt(i, 1).toString());
            path = f.getAbsolutePath();
            file = path.substring(path.lastIndexOf("\\") + 1, path.length());
            size = fileSize(Double.parseDouble(String.valueOf(f.length())));
            SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
            date = sdf.format(f.lastModified());
            if (model.getValueAt(i, 3).toString() == null ? date != null : !model.getValueAt(i, 3).toString().equals(date)) {
                model.setValueAt(date, i, 3);
                model.setValueAt(false, i, 4);
            }
            if (model.getValueAt(i, 2).toString() == null ? size != null : !model.getValueAt(i, 2).toString().equals(size)) {
                model.setValueAt(size, i, 2);
                model.setValueAt(false, i, 4);
            }
            BUFile bufile = new BUFile(login, file, size, date, path);
            //Do wysłania na serwer w celu sprawdzenia czy takie coś nie znajduje się w bazie
            String message = "fileName>>" + file + ">>" + size + ">>" + date;
            try {
                if (LoginFrame.client == null) {
                    LoginFrame.client = new Client(LoginFrame.HOST, LoginFrame.PORT);
                }
                LoginFrame.client.sendMessage(Client.socket, message);
                String tmp = LoginFrame.client.receiveMessage(Client.socket);
                switch (tmp) {
                    case "true":
                        bufile.setStatus(true);
                        break;
                    default:
                        String tmp1 = retriveData(file);
                        switch (tmp1) {
                            ///ZWRACA FALSE GDY JEST TAKI SAM 
                            case "true":
                                bufile.setActual(true);
                                bufile.setStatus(false);
                                break;

                            default:
                                bufile.setActual(false);
                                bufile.setStatus(false);
                                break;
                        }

                        break;
                }
            } catch (IOException ex) {
            }
            listOfFile.add(bufile);
        }
        if (listOfFile.isEmpty()) {
            infoLabel.setText("Files backuped.");
        }
    }
        
    /**
     * metoda stopująca wątek cyklicznego wysyłania danych
     */
    public void stopThread() {
        on.stop();
        stopVal = true;
        SchedulerCron.stopCron();
        buttonLoadFIle.setEnabled(true);
        buttonBackup.setEnabled(true);
        buttonLoadFIle.setEnabled(true);        
        buttonRecover.setEnabled(true);
        buttonScheluderOn.setEnabled(true);
        buttonScheluderOff.setEnabled(false);
        buttonDelete.setEnabled(true);
    }

    /**
     * Metoda pobierająca dane danego pliku
     * @param file
     * @return
     * @throws IOException 
     */
    public String retriveData(String file) throws IOException {
        String message1 = "oneFile>>" + file;
        if (LoginFrame.client == null) {
            LoginFrame.client = new Client(LoginFrame.HOST, LoginFrame.PORT);
        }
        LoginFrame.client.sendMessage(Client.socket, message1);
        String tmp1 = LoginFrame.client.receiveMessage(Client.socket);
        return tmp1;
    }

    public JLabel getLabelLogged(){
        return labelLogged;
    }
    public static boolean getStopVal() {
        return stopVal;
    }

    public JTable getServerTable() {
        return tableServer;
    }

    public JTable getClientTable() {
        return tableClient;
    }

    public JLabel getInfoLabel() {
        return infoLabel;
    }

    public JButton getBackupButton() {
        return buttonBackup;
    }

    public JButton getSchedulerButtonOn() {
        return buttonScheluderOn;
    }

    public JButton getSchedulerButtonOff() {
        return buttonScheluderOff;
    }

    public JButton getRecoverButton() {
        return buttonRecover;
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton buttonBackup;
    private javax.swing.JButton buttonDelete;
    private javax.swing.JButton buttonLoadFIle;
    private javax.swing.JButton buttonRecover;
    private javax.swing.JButton buttonScheluderOff;
    private javax.swing.JButton buttonScheluderOn;
    private javax.swing.JLabel infoLabel;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane4;
    private javax.swing.JScrollPane jScrollPane5;
    private javax.swing.JScrollPane jScrollPane6;
    private javax.swing.JScrollPane jScrollPane7;
    private javax.swing.JScrollPane jScrollPane8;
    private javax.swing.JLabel labelDayOfMonth;
    private javax.swing.JLabel labelDayOfWeek;
    private javax.swing.JLabel labelHour;
    private javax.swing.JLabel labelLogged;
    private javax.swing.JLabel labelMinute;
    private javax.swing.JLabel labelMonth;
    private javax.swing.JList listDayOfMonth;
    private javax.swing.JList listDayOfWeek;
    private javax.swing.JList listHour;
    private javax.swing.JList listMinute;
    private javax.swing.JList listMonth;
    private javax.swing.JTable tableClient;
    private javax.swing.JTable tableServer;
    // End of variables declaration//GEN-END:variables
}
