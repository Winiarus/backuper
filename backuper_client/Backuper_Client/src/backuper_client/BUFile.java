/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package backuper_client;

/**
 * Klasa reprezentująca pliki po stronie klienta
 */
public class BUFile {

    /**
     * Pole reprezentujące właściciela pliku
     */
    private String user;
    /**
     * Pole zawierające nazwę pliku
     */
    private String file;
    /**
     * Pole zawierające rozmiar pliku
     */
    private String size;
    /**
     * Pole zawierające datę ostatniej modyfikacji pliku
     */
    private String date;
    /**
     * Pole zawierające ścieżkę do pliku
     */
    private String path;
    /**
     * Pole zawierające informacje czy plik znajduje się na serwerze 
     * w momencie dodawania go do listy plików do wysłania na serwer
     */
    private Boolean status;
    /**
     * Pole zawierające informacje czy metadane pliku klienckiego są
     * takie same jak na serwerze
     */
    private Boolean actual;

    /**
     * konstruktor parametrowy klasy BUFile
     * @param user
     * @param file
     * @param size
     * @param date
     * @param path 
     */
    public BUFile(String user, String file, String size, String date, String path) {
        this.user = user;
        this.file = file;
        this.size = size;
        this.date = date;
        this.path = path;
    }

    public void setStatus(Boolean status){
        this.status = status;
    }
    
    public void setUser(String user) {
        this.user = user;
    }

    public void setFile(String file) {
        this.file = file;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public void setPath(String path) {
        this.path = path;
    }
    
    public void setActual(Boolean actual) {
        this.actual = actual;
    }
    
    public Boolean getActual() {
        return actual;
    }

    public Boolean getStatus() {
        return status;
    }
    
    public String getUser() {
        return user;
    }

    public String getFile() {
        return file;
    }

    public String getSize() {
        return size;
    }

    public String getDate() {
        return date;
    }

    public String getPath() {
        return path;
    }
    
    @Override
    public String toString() { 
        return "User: '" + this.user + "', File: '" + this.file + "', Path: '" + this.path + "', Size: '" + this.size + "', Date: '" + this.date + "', Status: '" + this.status + "'";
    } 
}
