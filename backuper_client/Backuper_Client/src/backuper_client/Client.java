package backuper_client;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;

/**
 * Klasa reprezentująca klienta połączenia poprzez gniazda
 */
public final class Client {

    public static Socket socket;

    /**
     * Konstruktor parametrowy klasy Client
     * @param hostname
     * @param port
     * @throws UnknownHostException
     * @throws IOException 
     */
    public Client(String hostname, int port) throws UnknownHostException, IOException {
        InetAddress address = InetAddress.getByName(hostname);
        socket = new Socket(address, port);
    }

    /**
     * Metoda wysyłające wiadomość wiadomość poprzez socket
     * @param client
     * @param message
     * @throws IOException 
     */
    public void sendMessage(Socket client, String message) throws IOException {
        OutputStream os = socket.getOutputStream();
        OutputStreamWriter osw = new OutputStreamWriter(os);
        BufferedWriter bw = new BufferedWriter(osw);
        String sendMessage = message + "\n";
        bw.write(sendMessage);
        bw.flush();
    }
    
    /**
     * Metoda odczytująca wiadomość, która przyszła z serwera
     * @param client
     * @return
     * @throws IOException 
     */
    public String receiveMessage(Socket client) throws IOException {
        InputStream is = socket.getInputStream();
        InputStreamReader isr = new InputStreamReader(is);
        BufferedReader br = new BufferedReader(isr);
        String message = br.readLine();
        return message;
    }
}
