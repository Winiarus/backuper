package backuper_server;

import java.sql.*;
import java.util.ArrayList;

/**
 * klasa komunikująca się z bazą danych
 */
public class DB {

    public static final String DBURL = "jdbc:sqlite:"+System.getProperty("user.dir")+"\\db\\DB_server.db";

    private Connection connection = null;
    private Statement statement = null;
    private ResultSet resultSet = null;
    private PreparedStatement preparedStatement = null;

    /**
     * kontruktor klasy DB
     */
    public DB() {
        try {
            Class.forName("org.sqlite.JDBC");
        } catch (ClassNotFoundException e) {
            System.err.println("Driver JDBC not found");
            e.printStackTrace();
        }
    }
    
    /**
     * Metoda łącząca się z bazą danych
     */
    public void connectDB(){
        try {
            // create a database connection
            connection = DriverManager.getConnection(DBURL);
            statement = connection.createStatement();
           // statement.setQueryTimeout(30);  // set timeout to 30 sec.
        }catch(SQLException e) {
            System.err.println("Cannot connect to database");
            e.printStackTrace();
        }
    }
    
    /**
     * Metoda rozłączająca się z bazą danych
     */
    public void disconnectDB(){
        try{
            if (preparedStatement != null)
                preparedStatement.close();
            if (resultSet != null)
                    resultSet.close();
            if (statement != null)
                    statement.close();
            if (connection != null)
                    connection.close();
        }catch(SQLException e) {
            System.err.println("Cannot disconnect to database");
        }
    }
    
    /**
     * Metoda pobierająca dane plików danego użytkownika
     * @param user
     * @return 
     */
    public String selectFiles(String user){
        connectDB();
        String s = "";
        int i=0;
        try{
            String query = "select * from Files where User_ID=(select ID from Users where Login='"+user+"')";
            resultSet = statement.executeQuery(query);
            if(resultSet==null){
                s = "0";
            } else {
                while (resultSet.next()) {
                    s += resultSet.getString("File") + ">>" + resultSet.getString("Size") + ">>" + resultSet.getString("Date") + ">>";
                    i++;
                }
                s = Integer.toString(i)+">>"+s+"\n";
            }
        } catch (SQLException e) {
            System.err.println(e.getMessage());
        } finally {
            disconnectDB();
        }
        return s;
    }
    
    /**
     * Metoda sprawdzająca czy podany user i hasło są zgodne
     * @param login
     * @param password
     * @return 
     */
    public boolean selectUser(String login, String password){
        boolean flag = false;
        connectDB();
        String query = "select count(*) as total from Users where Login='"+login+"' AND Pass='"+password+"'";
        try{
            resultSet = statement.executeQuery(query);
                       
            if(resultSet.getInt("total")==1){
                flag = true;
            }
        } catch (SQLException e) {
            System.err.println(e.getMessage());
        } finally {
            disconnectDB();
        }
        return flag;
    }
    
    /**
     * Metoda sprawdzająca czy plik z podanymi metadanymi jest w bazie danych
     * @param file
     * @param size
     * @param date
     * @return 
     */
    public boolean selectFile(String file, String size, String date){
        boolean flag = false;
        connectDB();
        String query = "select count(*) as total from Files where File='"+file+"' AND Size='"+size+"' AND Date='"+date+"'";
        try{
            resultSet = statement.executeQuery(query);  
            if(resultSet.getInt("total")!=0){
                flag = true;
            }
        } catch (SQLException e) {
            System.err.println(e.getMessage());
        } finally {
            disconnectDB();
        }
        return flag;
    }
    
    /**
     * Metoda sprawdzająca czy dany plik jest w bazie danych
     * @param file
     * @return 
     */
     public boolean oneFile(String file){
        boolean flag = false;
        connectDB();
        String query = "select count(*) as total from Files where File='"+file+"'";
        try{
            resultSet = statement.executeQuery(query);  
            if(resultSet.getInt("total")!=0){
                flag = true;
            }
        } catch (SQLException e) {
            System.err.println(e.getMessage());
        } finally {
            disconnectDB();
        }
        return flag;
    }
    
    /**
     * metoda dodająca plik do bazy danych
     * @param user
     * @param file
     * @param size
     * @param date
     * @return 
     */
    public boolean addFile(String user, String file, String size, String date){
        boolean flag = false;
        String userID = "";
        connectDB();
        
        String query = "Select ID from Users where Login='"+user+"'";
        try{
            resultSet = statement.executeQuery(query);
            while(resultSet.next()){
                userID = resultSet.getString("ID");
            }
            disconnectDB();
            connectDB();
            query = "Insert into Files (File, Size, Date, User_ID) Values ('"+file+"', '"+size+"', '"+date+"', '"+userID+"')";            
            statement.executeUpdate(query);
            flag = true;
        }catch (SQLException e) {
            System.err.println(e.getMessage());
        }finally {
            disconnectDB();
        }
        return flag;
    }

    /**
     * Metoda aktualizująca plik w bazie danych
     * @param user
     * @param file
     * @param size
     * @param date
     * @return 
     */
    public boolean updateFile(String user, String file, String size, String date){
        boolean flag = false;
        String userID = "";
        connectDB();
        
        String query = "Select ID from Users where Login='"+user+"'";
        try{
            resultSet = statement.executeQuery(query);
            while(resultSet.next()){
                userID = resultSet.getString("ID");
            }
            disconnectDB();
            connectDB();
            query = "Update Files Set File='"+file+"', Size='"+size+"', Date='"+date+"' WHERE File = '"+file+"' AND User_ID ='"+userID+"'";
            statement.executeUpdate(query);
            
            flag = true;
   
        }catch (SQLException e) {
            System.err.println(e.getMessage());
        }finally {
            disconnectDB();
        }
        return flag;
    }
}
